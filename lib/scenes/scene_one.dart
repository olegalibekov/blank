import 'dart:math';

import 'package:blank/scenes/scene_five.dart';
import 'package:blank/scenes/scene_seven.dart';
import 'package:blank/scenes/scene_three/scene_three.dart';
import 'package:blank/scenes/scene_two.dart';
import 'package:blank/screen.dart';
import 'package:blank/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:zflutter/zflutter.dart';

enum SceneOneEvents {
  backgroundSpace,
  startSequence,
  moonLightStudios,
  souzMultfilm,
  googleLogo,
  lucasFilm,
  warnerBros,
  southSideAmusementCompany
}

extension SceneOne on Screen {
  initSceneOne() {
    Screen.broadcaster.listen((event) {
      switch (event) {
        case SceneOneEvents.backgroundSpace:
          backgroundSpace();
          break;
        case SceneOneEvents.startSequence:
          startSequence();
          break;
        case SceneOneEvents.moonLightStudios:
          moonLightStudios();
          break;
        case SceneOneEvents.souzMultfilm:
          souzMultfilm();
          break;
        case SceneOneEvents.googleLogo:
          googleLogo();
          break;
        case SceneOneEvents.lucasFilm:
          lucasFilm();
          break;
        case SceneOneEvents.warnerBros:
          warnerBros();
          break;
        case SceneOneEvents.southSideAmusementCompany:
          southSideAmusementCompany();
          break;
      }
    });
  }

  startSequence([element]) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    if (kIsWeb) {
      //js.context.callMethod('openFullscreen'); //TODO remove after tests
    }
    Screen.stopwatch.start();
    startSequenceWidget = Container(key: UniqueKey());
    Screen.brain.add(element ?? SceneOneEvents.backgroundSpace);
  }

  backgroundSpace() {
    backgroundWidget =
        animatedSwitcher(StarSky(), animationDurationX5, Curves.easeInCubic);
    updateScreen();
    addTimer(animationDurationX7,
        () => Screen.brain.add(SceneOneEvents.moonLightStudios));
  }

  moonLightStudios() async {
    Key key = GlobalKey();

    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: animationDurationX3));

    //Screen.animationControllers[animationControllerKey] = animationController;

    Animation tween = Tween(begin: .0, end: 1.0).animate(animationController.c);

    positionedWidgets["moonlightStudios"] = IgnorePointer(
      key: key,
      child: AnimatedBuilder(
        animation: animationController.c,
        child: Center(
          child: SizedBox(
              width: 300,
              child: Image.network(
                  "https://live.staticflickr.com/4317/35129607344_fe8f311d79_k.jpg")),
        ),
        builder: (BuildContext context, Widget child) {
          return Opacity(
            opacity: tween.value,
            child: child,
          );
        },
      ),
    );
    updateScreen();
    animationController.c.duration = Duration(seconds: 2);
    await animationController.c.forward();
    animationController.c.duration = Duration(seconds: 2);
    await animationController.c.reverse();
    animationController.close();
    key = null;
    positionedWidgets.remove("moonlightStudios");
    addTimer(animationDurationX7,
        () => Screen.brain.add(SceneOneEvents.souzMultfilm));

    /// Должна добавить текст на экран, сделать это с помощью анимации.
  }

  souzMultfilm() async {
    ///Первая ступень - создание подобного рода ключа
    Key key = GlobalKey();

    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: animationDurationX3));

    //Screen.animationControllers[animationControllerKey] = animationController;

    Animation tween = Tween(begin: .0, end: 1.0).animate(animationController.c);

    positionedWidgets["souzMultfilm"] = IgnorePointer(
      key: key,
      child: AnimatedBuilder(
        animation: animationController.c,
        child: Center(
          child: SizedBox(
              width: 300,
              child: Image.network(
                  "https://ruvod.com/wp-content/uploads/2019/11/sm_new_logo.jpg")),
        ),
        builder: (BuildContext context, Widget child) {
          return Opacity(
            opacity: tween.value,
            child: child,
          );
        },
      ),
    );
    updateScreen();
    animationController.c.duration = Duration(seconds: 3);
    await animationController.c.forward();
    animationController.c.duration = Duration(seconds: 2);
    await animationController.c.reverse();
    animationController.close();
    key = null;
    positionedWidgets.remove("souzMultfilm");
    addTimer(
        animationDurationX7, () => Screen.brain.add(SceneOneEvents.googleLogo));

    /// Должна добавить текст на экран, сделать это с помощью анимации.
  }

  googleLogo() async {
    ///Первая ступень - создание подобного рода ключа
    Key key = GlobalKey();

    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: animationDurationX3));

    //Screen.animationControllers[animationControllerKey] = animationController;

    Animation tween = Tween(begin: .0, end: 1.0).animate(animationController.c);

    positionedWidgets["googleLogo"] = IgnorePointer(
      key: key,
      child: AnimatedBuilder(
        animation: animationController.c,
        child: Center(
          child: SizedBox(
              width: 300,
              child: Image.network(
                  "https://pngimg.com/uploads/google/google_PNG19642.png")),
        ),
        builder: (BuildContext context, Widget child) {
          return Opacity(
            opacity: tween.value,
            child: child,
          );
        },
      ),
    );
    updateScreen();
    animationController.c.duration = Duration(seconds: 3);
    await animationController.c.forward();
    animationController.c.duration = Duration(seconds: 2);
    await animationController.c.reverse();
    animationController.close();
    key = null;
    positionedWidgets.remove("googleLogo");
    addTimer(
        animationDurationX7, () => Screen.brain.add(SceneOneEvents.lucasFilm));

    /// Должна добавить текст на экран, сделать это с помощью анимации.
  }

  lucasFilm() async {
    ///Первая ступень - создание подобного рода ключа
    Key key = GlobalKey();

    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: animationDurationX3));

    //Screen.animationControllers[animationControllerKey] = animationController;

    Animation tween = Tween(begin: .0, end: 1.0).animate(animationController.c);

    positionedWidgets["lucasFilm"] = IgnorePointer(
      key: key,
      child: AnimatedBuilder(
        animation: animationController.c,
        child: Center(
          child: SizedBox(
              width: 300,
              child: Image.network(
                  "http://goliquidsoul.com/wp-content/uploads/2015/03/Lucasfilm.png                                                                                       ")),
        ),
        builder: (BuildContext context, Widget child) {
          return Opacity(
            opacity: tween.value,
            child: child,
          );
        },
      ),
    );
    updateScreen();
    animationController.c.duration = Duration(seconds: 3);
    await animationController.c.forward();
    animationController.c.duration = Duration(seconds: 2);
    await animationController.c.reverse();
    animationController.close();
    key = null;
    positionedWidgets.remove("lucasFilm");
    addTimer(
        animationDurationX7, () => Screen.brain.add(SceneOneEvents.warnerBros));

    /// Должна добавить текст на экран, сделать это с помощью анимации.
  }

  warnerBros() async {
    ///Первая ступень - создание подобного рода ключа
    Key key = GlobalKey();

    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: animationDurationX3));

    //Screen.animationControllers[animationControllerKey] = animationController;

    Animation tween = Tween(begin: .0, end: 1.0).animate(animationController.c);

    positionedWidgets["warnerBros"] = IgnorePointer(
      key: key,
      child: AnimatedBuilder(
        animation: animationController.c,
        child: Center(
          child: SizedBox(
              width: 400,
              child: Image.network(
                  "http://logok.org/wp-content/uploads/2014/12/Warner_Bros_logo.svg_.png")),
        ),
        builder: (BuildContext context, Widget child) {
          return Opacity(
            opacity: tween.value,
            child: child,
          );
        },
      ),
    );
    updateScreen();
    animationController.c.duration = Duration(seconds: 3);
    await animationController.c.forward();
    animationController.c.duration = Duration(seconds: 2);
    await animationController.c.reverse();
    animationController.close();
    key = null;
    positionedWidgets.remove("warnerBros");
    addTimer(animationDurationX7,
        () => Screen.brain.add(SceneOneEvents.southSideAmusementCompany));

    /// Должна добавить текст на экран, сделать это с помощью анимации.
  }

  southSideAmusementCompany() async {
    ///Первая ступень - создание подобного рода ключа
    Key key = GlobalKey();

    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: animationDurationX3));

    //Screen.animationControllers[animationControllerKey] = animationController;

    Animation tween = Tween(begin: .0, end: 1.0).animate(animationController.c);

    positionedWidgets["southSideAmusementCompany"] = IgnorePointer(
      key: key,
      child: AnimatedBuilder(
        animation: animationController.c,
        child: Center(
          child: SizedBox(
              width: 400,
              child: Image.network(
                  "https://www.palaceamusement.com/assets/img/banner-logo.png")),
        ),
        builder: (BuildContext context, Widget child) {
          return Opacity(
            opacity: tween.value,
            child: child,
          );
        },
      ),
    );
    updateScreen();
    animationController.c.duration = Duration(seconds: 3);
    await animationController.c.forward();
    animationController.c.duration = Duration(seconds: 2);
    await animationController.c.reverse();
    animationController.close();
    key = null;
    positionedWidgets.remove("southSideAmusementCompany");

    /// Должна добавить текст на экран, сделать это с помощью анимации.
  }
}

class StartSequence extends StatelessWidget {
  const StartSequence({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: FlatButton(
        onPressed: () {
          Screen.brain.add(SceneOneEvents.startSequence);
        },
        child: Text('Start'),
      ),
    );
  }
}

class StarSky extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.black,
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: Image.asset('images/background/star_sky.png',
            repeat: ImageRepeat.repeat));
  }
}
