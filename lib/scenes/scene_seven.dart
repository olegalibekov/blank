import 'dart:async';
import 'dart:js';
import 'dart:math';

import '../render_lib/shapes.dart';
import '../render_lib/point_3d.dart';
import 'package:blank/render_lib/object3d.dart';
import 'package:blank/screen.dart';
import 'package:blank/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/services.dart';
import 'dart:ui' as ui;

import 'package:vector_math/vector_math.dart' hide Colors;

import 'package:zflutter/zflutter.dart';

enum SceneSevenEvents { backgroundSpace, startSceneSeven, mosaic }

Random random = new Random();

String objectPath = 'images/sceneFive/round_spaceship_v1.obj';

String bunnyPath =
    'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4';

int next(int min, int max) => min + random.nextInt(max - min);

extension SceneSeven on Screen {
  initSceneSeven() {
    Screen.broadcaster.listen((event) {
      switch (event) {
        case SceneSevenEvents.startSceneSeven:
          startSceneSeven();
          //asPeachTree();
          break;
        case SceneSevenEvents.mosaic:
          asPeachTree();
          break;
      }
    });
  }

  startSceneSeven() {
    backgroundWidget = Container(
      color: Colors.white,
    );
    updateScreen();
    Screen.brain.add(SceneSevenEvents.mosaic);
  }

  asPeachTree() async {
    Point firstVideoCenter = Point(250, 150);
    Point leftUpperVideoFirst =
        Point(firstVideoCenter.x - 250, firstVideoCenter.y + 250 / 1.77);
    Point rightDownVideoFirst =
        Point(firstVideoCenter.x + 250, firstVideoCenter.y - 250 / 1.77);

    Point secondVideoCenter = Point(-350, 150);
    Point leftUpperVideoSecond =
        Point(secondVideoCenter.x - 250, secondVideoCenter.y + 250 / 1.77);
    Point rightDownVideoSecond =
        Point(secondVideoCenter.x + 250, secondVideoCenter.y - 250 / 1.77);

    Point thirdVideoCenter = Point(-450, -350);
    Point leftUpperVideoThird =
        Point(thirdVideoCenter.x - 250, thirdVideoCenter.y + 250 / 1.77);
    Point rightDownVideoThird =
        Point(thirdVideoCenter.x + 250, thirdVideoCenter.y - 250 / 1.77);

//
//    Point leftUpperVideoSecond = Point(-600, -200);
//    Point rightDownVideoSecond = Point(-100, -100);
//
//    Point startCheckCenters = Point(min(leftUpperVideoFirst.x, rightDownVideoFirst.x), min(leftUpperVideoFirst.y, rightDownVideoFirst.y));
//    Point endCheckCenters = Point(max(leftUpperVideoFirst.x, rightDownVideoFirst.x), max(leftUpperVideoFirst.y, rightDownVideoFirst.y));

    //Mosaic creating
    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 10)));

    Point3D Q;

    List<AControllerWrapper> mainBlocControllers = [];
    List<Widget> mainBlocs = [];

    for (int i = 0; i < 40; i++) {
      AControllerWrapper mosaicSquareController = AControllerWrapper(
          AnimationController(
              vsync: vsync,
              duration: Duration(milliseconds: 200 + random.nextInt(400))));

      Tween mosaicTween = Tween(begin: 1, end: 0);

      Animation mosaicAnimationTween =
          mosaicTween.animate(mosaicSquareController.c);

      mainBlocControllers.add(mosaicSquareController);

      mainBlocs.add(AnimatedBuilder(
        animation: mosaicSquareController.c,
        builder: (BuildContext context, Widget child) {
          return ZGroup(
            children: [
              ...Shapes.quadrilateralSides(
                  Point3D(0, 0, 0),
                  MediaQuery.of(globalContext).size.width / 16 / 5,
                  (MediaQuery.of(globalContext).size.height / 9 / 5) + 2,
// Полосы поперёк экрана. Разница между координатами верхних правых углов такая же
//                  MediaQuery.of(globalContext).size.height / 20,
//                  MediaQuery.of(globalContext).size.height / 20,

// Полосы поперёк экрана^
                  mosaicAnimationTween.value),
            ],
          );
        },
      ));
    }

    List<Widget> noOpacity = [];
    noOpacity.add(ZGroup(
      children: [
        ...Shapes.quadrilateralSides(
            Point3D(0, 0, 0),
            MediaQuery.of(globalContext).size.width / 16 / 5,
            (MediaQuery.of(globalContext).size.height / 9 / 5) + 2,
            1),
      ],
    ));

    double radius;
    List mosaic = [];

    //Adding mosaic for first video
    radius = 110;
    mosaic.clear();

    for (double i = leftUpperVideoFirst.x - 5;
        i < rightDownVideoFirst.x + 5;
        i += MediaQuery.of(globalContext).size.width / 16 / 5) {
      for (double j = rightDownVideoFirst.y - 5;
          j < leftUpperVideoFirst.y + 5;
          j += MediaQuery.of(globalContext).size.height / 9 /  5) {
        if (Point3D.distTwoPoints(
                    Point3D(firstVideoCenter.x, firstVideoCenter.y, 0),
                    Point3D(i, j, 0)) +
                random.nextDouble() * radius - radius / 1.2<
            radius) {
          mosaic.add(ZPositioned(
            translate: ZVector(i, j, 0),
            child: mainBlocs.elementAt(random.nextInt(mainBlocs.length - 1)),
          ));
        } else {
          mosaic.add(ZPositioned(
            translate: ZVector(i, j, 0),
            child: noOpacity.elementAt(0),
          ));
        }
        /*
        else {
          if (((i >= leftUpperVideoSecond.x) &&
              (i <= rightDownVideoSecond.x) &&
              (j >= leftUpperVideoSecond.y) &&
              (j <= rightDownVideoSecond.y))) {
            mosaic.add(ZPositioned(
              translate: ZVector(i, j, 0),
              child: mainBlocs.elementAt(random.nextInt(mainBlocs.length - 1)),
            ));
          } else {
            mosaic.add(ZPositioned(
              translate: ZVector(i, j, 0),
              child: noOpacity.elementAt(0),
            ));
          }
        }
        */
      }
    }

    //Adding mosaic for second video
    radius = 100;

    for (double i = leftUpperVideoSecond.x - 5;
        i < rightDownVideoSecond.x + 5;
        i += MediaQuery.of(globalContext).size.width / 16 /  5) {
      for (double j = rightDownVideoSecond.y + 5;
          j < leftUpperVideoSecond.y - 5;
          j += MediaQuery.of(globalContext).size.height / 9 /  5) {
        if (Point3D.distTwoPoints(
                    Point3D(secondVideoCenter.x, secondVideoCenter.y, 0),
                    Point3D(i, j, 0)) +
                random.nextDouble() * radius - radius / 1.2 <
            radius) {
          mosaic.add(ZPositioned(
            translate: ZVector(i, j, 0),
            child: mainBlocs.elementAt(random.nextInt(mainBlocs.length - 1)),
          ));
        } else {
          mosaic.add(ZPositioned(
            translate: ZVector(i, j, 0),
            child: noOpacity.elementAt(0),
          ));
        }
      }
    }

    //Adding mosaic for third video
    radius = 120;

    for (double i = leftUpperVideoThird.x - 5;
        i < rightDownVideoThird.x + 5;
        i += MediaQuery.of(globalContext).size.width / 16 / 5) {
      for (double j = rightDownVideoThird.y + 2;
          j < leftUpperVideoThird.y - 2;
          j += MediaQuery.of(globalContext).size.height / 9 / 5) {
        if (Point3D.distTwoPoints(
                    Point3D(thirdVideoCenter.x, thirdVideoCenter.y, 0),
                    Point3D(i, j, 0)) +
                random.nextDouble() * radius - radius / 1.2 <
            radius) {
          mosaic.add(ZPositioned(
            translate: ZVector(i, j, 0),
            child: mainBlocs.elementAt(random.nextInt(mainBlocs.length - 1)),
          ));
        } else {
          mosaic.add(ZPositioned(
            translate: ZVector(i, j, 0),
            child: noOpacity.elementAt(0),
          ));
        }
      }
    }

    AControllerWrapper videoController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 1)));

    Tween videoMove = Tween(begin: 0, end: 1);

    Animation capsuleMoveAnimationTween = videoMove.animate(videoController.c);

    videoController.c.forward();
    VideoPlayerController _controller = VideoPlayerController.network(
        'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4');

    await _controller
      ..initialize()
      ..play();
    _controller.setVolume(0);

    VideoPlayerController _controller2 = VideoPlayerController.network(
        'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4');

    await _controller2
      ..initialize()
      ..play();
    _controller.setVolume(0);

    VideoPlayerController _controller3 = VideoPlayerController.network(
        'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4');

    await _controller3
      ..initialize()
      ..play();
    _controller.setVolume(0);

    zWidgets["mosaic"] = ZGroup(sortMode: SortMode.stack, children: [
      // First Video
      AnimatedBuilder(
        child: SizedBox.expand(
          child: FittedBox(
            fit: BoxFit.cover,
            child: SizedBox(
              width: 500,
              //width: 500,
              height: 500 / _controller.value.aspectRatio,
              child: VideoPlayer(_controller),
            ),
          ),
        ),
        animation: videoController.c,
        builder: (BuildContext context, Widget child) {
          _controller.setVolume(capsuleMoveAnimationTween.value);
          return ZPositioned(
            translate: ZVector(250, 150, 0),
            rotate: ZVector(0, 0, 0),
            child: ZToBoxAdapter(
              height: 500 / _controller.value.aspectRatio,
              width: 500,
//              height: MediaQuery.of(context).size.height,
//              width: MediaQuery.of(context).size.width,
              child: SizedBox(
                width: 500,
                height: 500 / _controller.value.aspectRatio,
                child: child,
              ),
            ),
          );
        },
      ),

      //Second Video
      AnimatedBuilder(
        child: SizedBox.expand(
          child: FittedBox(
            fit: BoxFit.cover,
            child: SizedBox(
              width: 500,
              //width: 500,
              height: 500 / _controller2.value.aspectRatio,
              child: VideoPlayer(_controller2),
            ),
          ),
        ),
        animation: videoController.c,
        builder: (BuildContext context, Widget child) {
          _controller2.setVolume(capsuleMoveAnimationTween.value);
          return ZPositioned(
            translate: ZVector(-350, 150, 0),
            rotate: ZVector(0, 0, 0),
            child: ZToBoxAdapter(
              height: 500 / _controller2.value.aspectRatio,
              width: 500,
//              height: MediaQuery.of(context).size.height,
//              width: MediaQuery.of(context).size.width,
              child: SizedBox(
                width: 500,
                height: 500 / _controller2.value.aspectRatio,
                child: child,
              ),
            ),
          );
        },
      ),

      //Third Video
      AnimatedBuilder(
        child: SizedBox.expand(
          child: FittedBox(
            fit: BoxFit.cover,
            child: SizedBox(
              width: 500,
              //width: 500,
              height: 500 / _controller3.value.aspectRatio,
              child: VideoPlayer(_controller3),
            ),
          ),
        ),
        animation: videoController.c,
        builder: (BuildContext context, Widget child) {
          _controller3.setVolume(capsuleMoveAnimationTween.value);
          return ZPositioned(
            translate: ZVector(-450, -350, 0),
            rotate: ZVector(0, 0, 0),
            child: ZToBoxAdapter(
              height: 500 / _controller3.value.aspectRatio,
              width: 500,
//              height: MediaQuery.of(context).size.height,
//              width: MediaQuery.of(context).size.width,
              child: SizedBox(
                width: 500,
                height: 500 / _controller3.value.aspectRatio,
                child: child,
              ),
            ),
          );
        },
      ),

      ...mosaic,
    ]);

    updateScreen();
    mainBlocControllers.shuffle();
    for (var i in mainBlocControllers) {
      await Future.delayed(Duration(milliseconds: random.nextInt(300)));
      i.c.forward();
    }

    await animationController.c.forward();

    zWidgets.remove("peachTree");
    updateScreen();
    await Future.delayed(Duration(milliseconds: 10));
    //fallingIntoPeachTree();
  }
}
