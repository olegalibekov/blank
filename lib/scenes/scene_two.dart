import 'package:blank/scenes/scene_three/scene_three.dart';
import 'package:blank/screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zflutter/zflutter.dart';

import '../utilities.dart';

enum SceneTwoEvents { startSceneTwo, shipMove }

extension SceneSecond on Screen {
  initSceneTwo() {
    Screen.broadcaster.listen((event) {
      switch (event) {
        case SceneTwoEvents.startSceneTwo:
          startSceneTwo();
          break;
        case SceneTwoEvents.shipMove:
          shipMove();
          break;
      }
    });
  }

  startSceneTwo() {
    backgroundWidget = Container(color: Colors.black);
    updateScreen();
    Screen.brain.add(SceneTwoEvents.shipMove);
  }

  shipMove() {
    vzWidgets['spaceship'] = ZToBoxAdapter(
        width: 200,
        height: 200,
        child: Opacity(
            opacity: 0.45,
            child: Image.asset('images/scene_two/star_ship.png',
                color: Colors.transparent,
                colorBlendMode: BlendMode.color,
                width: 200,
                height: 200)));

    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 60)));
    Tween xPositionTween = Tween(begin: 650.0, end: 370.0);
    Tween yPositionTween = Tween(begin: -70.0, end: 100.0);
    Tween zPositionTween = Tween(begin: 40, end: 70.0);
    Animation xAnimationTween = xPositionTween.animate(animationController.c);
    Animation yAnimationTween = yPositionTween.animate(animationController.c);
    Animation zAnimationTween = zPositionTween.animate(animationController.c);

    zWidgets['spaceMove'] = AnimatedBuilder(
        animation: animationController.c,
        child: vzWidgets['spaceship'],
        builder: (BuildContext context, Widget child) {
          return ZPositioned(
              scale: ZVector.getScale(zAnimationTween.value),
              translate: ZVector(xAnimationTween.value, yAnimationTween.value,
                  zAnimationTween.value),
              child: child);
        });

    updateScreen();
    animationController.c.forward();
    Screen.brain.add(SceneThreeEvents.startSceneThree);
  }
}
