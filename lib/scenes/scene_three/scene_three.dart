import 'dart:convert';

import 'package:blank/screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:zflutter/zflutter.dart';

import 'point_3d.dart';
import 'shine_source.dart';

enum SceneThreeEvents { startSceneThree, lightObject }

extension SceneThree on Screen {
  initSceneThree() {
    Screen.broadcaster.listen((event) {
      switch (event) {
        case SceneThreeEvents.startSceneThree:
          startSceneThree();
          break;
        case SceneThreeEvents.lightObject:
          lightObject();
          break;
      }
    });
  }

  startSceneThree() {
    backgroundWidget = Container(color: Colors.black);
    loadJsonData();
    updateScreen();
    Screen.brain.add(SceneThreeEvents.lightObject);
  }

  Future<Map<String, dynamic>> loadJsonData() async {
    var jsonText =
        await rootBundle.loadString('images/sceneThree/spaceShip/objects.json');
    return json.decode(jsonText);
  }

  lightObject() {
    Map lightData;

    List shapeList = [];

    addShape(Point3D A, Point3D B, Point3D C, Point3D D) {
      Point3D I = Point3D(100, -50, -400);

      Point3D strokeA = Point3D().strokePoint(A, I);
      Point3D strokeB = Point3D().strokePoint(B, I);
      Point3D strokeC = Point3D().strokePoint(C, I);
      Point3D strokeD = Point3D().strokePoint(D, I);

      shapeList.add(ShineSource(I).plane(A, B));
      shapeList.add(ShineSource(I).plane(B, C));
      shapeList.add(ShineSource(I).plane(C, D));
      shapeList.add(ShineSource(I).plane(D, A));

      shapeList.add(ShineSource().pathShape([A, B, C, D]));
      shapeList
          .add(ShineSource().pathShape([strokeA, strokeB, strokeC, strokeD]));
    }

    initLightObject() {
      var windowsTriangle = lightData['objects']['figures']['triangles'][0];
      var windowsRect = lightData['objects']['figures']['windows'];

      vzWidgets['windowsPicture'] = ZToBoxAdapter(
          width: lightData['information']['width'],
          height: lightData['information']['height'],
          child: Image.asset(lightData['information']['image']));

      vzWidgets['windowsTriangle'] = ZShape(path: [
        ZMove.only(x: windowsTriangle[0]['x'], y: windowsTriangle[0]['y']),
        ZLine.only(x: windowsTriangle[1]['x'], y: windowsTriangle[1]['y']),
        ZLine.only(x: windowsTriangle[2]['x'], y: windowsTriangle[2]['y']),
      ], closed: true, stroke: 0, fill: true, color: Colors.blue);
      addShape(
          Point3D(windowsRect[0][0]['x'], windowsRect[0][0]['y'], 0),
          Point3D(windowsRect[0][1]['x'], windowsRect[0][1]['y'], 0),
          Point3D(windowsRect[0][2]['x'], windowsRect[0][2]['y'], 0),
          Point3D(windowsRect[0][3]['x'], windowsRect[0][3]['y'], 0));
      addShape(
          Point3D(windowsRect[1][0]['x'], windowsRect[1][0]['y'], 0),
          Point3D(windowsRect[1][1]['x'], windowsRect[1][1]['y'], 0),
          Point3D(windowsRect[1][2]['x'], windowsRect[1][2]['y'], 0),
          Point3D(windowsRect[1][3]['x'], windowsRect[1][3]['y'], 0));
      addShape(
          Point3D(windowsRect[2][0]['x'], windowsRect[2][0]['y'], 0),
          Point3D(windowsRect[2][1]['x'], windowsRect[2][1]['y'], 0),
          Point3D(windowsRect[2][2]['x'], windowsRect[2][2]['y'], 0),
          Point3D(windowsRect[2][3]['x'], windowsRect[2][3]['y'], 0));
      addShape(
          Point3D(windowsRect[3][0]['x'], windowsRect[3][0]['y'], 0),
          Point3D(windowsRect[3][1]['x'], windowsRect[3][1]['y'], 0),
          Point3D(windowsRect[3][2]['x'], windowsRect[3][2]['y'], 0),
          Point3D(windowsRect[3][3]['x'], windowsRect[3][3]['y'], 0));
      vzWidgets['windowsLeftUpperCorner'] = ZShape(path: [
        ZMove.only(x: windowsRect[0][0]['x'], y: windowsRect[0][0]['y']),
        ZLine.only(x: windowsRect[0][1]['x'], y: windowsRect[0][1]['y']),
        ZLine.only(x: windowsRect[0][2]['x'], y: windowsRect[0][2]['y']),
        ZLine.only(x: windowsRect[0][3]['x'], y: windowsRect[0][3]['y']),
      ], closed: true, stroke: 0, fill: true, color: Colors.cyan);
      vzWidgets['windowsRightUpperCorner'] = ZShape(path: [
        ZMove.only(x: windowsRect[1][0]['x'], y: windowsRect[1][0]['y']),
        ZLine.only(x: windowsRect[1][1]['x'], y: windowsRect[1][1]['y']),
        ZLine.only(x: windowsRect[1][2]['x'], y: windowsRect[1][2]['y']),
        ZLine.only(x: windowsRect[1][3]['x'], y: windowsRect[1][3]['y']),
      ], closed: true, stroke: 0, fill: true, color: Colors.orange);
      vzWidgets['windowsRightBottomCorner'] = ZShape(path: [
        ZMove.only(x: windowsRect[2][0]['x'], y: windowsRect[2][0]['y']),
        ZLine.only(x: windowsRect[2][1]['x'], y: windowsRect[2][1]['y']),
        ZLine.only(x: windowsRect[2][2]['x'], y: windowsRect[2][2]['y']),
        ZLine.only(x: windowsRect[2][3]['x'], y: windowsRect[2][3]['y']),
      ], closed: true, stroke: 0, fill: true, color: Colors.green);
      vzWidgets['windowsLeftBottomCorner'] = ZShape(path: [
        ZMove.only(x: windowsRect[3][0]['x'], y: windowsRect[3][0]['y']),
        ZLine.only(x: windowsRect[3][1]['x'], y: windowsRect[3][1]['y']),
        ZLine.only(x: windowsRect[3][2]['x'], y: windowsRect[3][2]['y']),
        ZLine.only(x: windowsRect[3][3]['x'], y: windowsRect[3][3]['y']),
      ], closed: true, stroke: 0, fill: true, color: Colors.indigo);

      final Gradient gradient = RadialGradient(colors: <Color>[
        Colors.yellow.withOpacity(1.0),
        Colors.yellow.withOpacity(.0)
      ], stops: [
        0.0,
        1
      ]);
      Rect rect = Rect.fromCircle(center: Offset(690, 1160), radius: 1000);
      Shader shader = gradient.createShader(rect);

      zWidgets['lightComponents'] = ZGroup(children: [
        vzWidgets['windowsPicture'],

//        vzWidgets['windowsLeftUpperCorner'],
//        vzWidgets['windowsRightUpperCorner'],
//        vzWidgets['windowsRightBottomCorner'],
//        vzWidgets['windowsLeftBottomCorner'],
        vzWidgets['windowsTriangle'],
        for (dynamic shape in shapeList) shape,
      ]);

      updateScreen();
    }

    getLightData() async {
      lightData = await loadJsonData();
      lightData = lightData['pictures'][0];
      initLightObject();
    }

    getLightData();
  }
}
