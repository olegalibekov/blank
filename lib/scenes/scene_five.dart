import 'dart:async';
import 'dart:math';

import 'package:blank/render_lib/object3d.dart';
import 'package:blank/screen.dart';
import 'package:blank/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/services.dart';
import 'dart:ui' as ui;

//import 'dart:vector_math';
import 'package:vector_math/vector_math.dart' hide Colors;

import 'package:zflutter/zflutter.dart';

enum SceneFiveEvents { backgroundSpace, startSceneFive, stormClouds }

Random random = new Random();

//String objectPath = 'images/sceneFive/spaceship2.obj';

String objectPath = 'images/sceneFive/round_spaceship_v1.obj';

String bunnyPath =
    'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4';

int next(int min, int max) => min + random.nextInt(max - min);

extension SceneFive on Screen {
  initSceneFive() {
    Screen.broadcaster.listen((event) {
      switch (event) {
        case SceneFiveEvents.startSceneFive:
          startSceneFive();
          break;
        case SceneFiveEvents.stormClouds:
          stormClouds();
          break;
      }
    });
  }

  startSceneFive() {
    backgroundWidget = Container(
      color: Colors.black,
    );
    updateScreen();
    Screen.brain.add(SceneFiveEvents.stormClouds);
  }

  stormClouds() async {
    List<Point> centers = [];
    Point curCenter = Point(0, 0);
    double radius = 200;

    for (int i = 0; i < 360; i += 8) {
      centers.add(Point(curCenter.x + radius * cos(i * 2 * pi / 360),
          curCenter.y + radius * sin(i * 2 * pi / 360)));
    }
    List<Widget> widgets = [];
    for (int i = 0; i < 45; i++) {
      widgets.add(ZPositioned(
          translate: ZVector(centers[i].x + 200 - random.nextInt(400),
              centers[i].y, random.nextInt(50).toDouble()),
          rotate: ZVector(2, 0, random.nextDouble()),
          child: ZToBoxAdapter(
            width: 256,
            height: 256,
            child: Image.asset(
              "images/sceneFive/smoke.png",
            ),
          )));
    }

    Key key = GlobalKey();

    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 20 * 2)));

    Tween tween = Tween(begin: -.2, end: .4);
    Animation animationTween = tween.animate(animationController.c);

    zWidgets["clouds"] = AnimatedBuilder(
      key: key,
      animation: animationController.c,
      child: ZGroup(
        children: widgets,
      ),
      builder: (BuildContext context, Widget child) {
        return ZPositioned(
            translate: ZVector(0, MediaQuery.of(context).size.height / 2, 0),
            rotate: ZVector(pi / 1.8, 0, animationTween.value),
            child: child);
      },
    );
    updateScreen();
    animationController.c.forward();
    frameTwoCapsule().then((value) {
      animationController.close();
      zWidgets.remove("clouds");
      key = null;
      updateScreen();
      spaceCube();
      //bunnyVideo();
    });

//    while((await Screen.broadcaster.firstWhere((element) => element=="secondCadr"))){
//
//    }
  }

  Future frameTwoCapsule() async {
    Key key = GlobalKey();

    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 2)));

    Tween tween = Tween(
        begin: -MediaQuery.of(globalContext).size.height / 2,
        end: MediaQuery.of(globalContext).size.height / 2);
    Animation animationTween = tween.animate(animationController.c);

    zWidgets["capsule"] = AnimatedBuilder(
      key: key,
      animation: animationController.c,
      child: ZToBoxAdapter(
        width: 20,
        height: 20,
        child: SpaceShip(),
      ),
      builder: (BuildContext context, Widget child) {
        return ZPositioned(
            //rotate: ZVector(0,0,random.nextDouble()),
            translate: ZVector(next(-1, 2).toDouble(), animationTween.value, 0),
            child: child);
      },
    );
    updateScreen();
    await animationController.c.forward();
    zWidgets.remove("capsule");
    key = null;
    return;
  }

  spaceCube() async {
//    Key key = GlobalKey();
    double radius = 750;
    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 10)));

    Tween tween = Tween(
        begin: (MediaQuery.of(globalContext).size.height + radius * 2),
        end: (MediaQuery.of(globalContext).size.height / 2 - radius / 2));
    Animation animationTween = tween.animate(animationController.c);

    Animation animationTween2 =
        Tween(begin: -140, end: 190.0).animate(animationController.c);

//    Animation animationTween2 =
//    Tween(begin: -140, end: 140.0).animate(animationController.c);

    Animation animationTween3 = TweenSequence([
      TweenSequenceItem(
        tween: Tween(begin: -140.0, end: -20.0),
        weight: 100,
      ),
      TweenSequenceItem(
        tween: Tween(begin: -20.0, end: 20.0),
        weight: 1,
      ),
      TweenSequenceItem(
        tween: Tween(begin: 20.0, end: 140.0),
        weight: 100,
      )
    ]).animate(animationController.c);

    Animation animationTween4 = Tween(begin: -140, end: 140.0).animate(
        CurvedAnimation(
            parent: animationController.c, curve: Curves.easeInOutQuad));

    Animation animateClouds = Tween(
            begin: (MediaQuery.of(globalContext).size.height + radius * 2),
            end: (MediaQuery.of(globalContext).size.height / 2 - radius / 2))
        .animate(CurvedAnimation(
            parent: animationController.c, curve: Curves.easeInOutExpo));

    List<Point> centers = [];
    Point curCenter = Point(0, 0);

    for (int i = 0; i < 360; i += 8) {
      centers.add(Point(
          curCenter.x +
              radius * cos(i * 2 * pi / 360) +
              random.nextInt((500)).toDouble() -
              250,
          curCenter.y +
              radius * sin(i * 2 * pi / 360) +
              random.nextInt((500)).toDouble() -
              250));
    }
    List<Widget> widgets = [];
    for (int i = 0; i < 45; i++) {
      widgets.add(ZPositioned(
          translate: ZVector(
              centers[i].x, centers[i].y, random.nextInt(50).toDouble()),
          //rotate: ZVector(2, 0, random.nextDouble()),
          child: ZToBoxAdapter(
            width: 800,
            height: 800,
            child: Image.asset("images/sceneFive/smoke.png",
                fit: BoxFit.fill, width: 800, height: 800),
          )));
      //widgets.add( position: clouds(centers[i]), rotation: i * 4  );
    }

//    zWidgets["cube"] = AnimatedBuilder(
//      animation: animationController.c,
//      child: ZGroup(children: [...widgets]),
//      builder: (context, child) {
//        return ZPositioned(
//          rotate: ZVector(-(pi) - pi / 8 - (pi / 8) * ((animationTween.value) / radius), 0, 0),
//          translate: ZVector(0, animationTween.value, 0),
//          child: child,
//        );
//      },
//
//    );

//    rotate: ZVector(pi / 4, 0, 0),
//      translate: ZVector(
//          0, (MediaQuery.of(globalContext).size.height / 2 - radius / 2), 0),

    AControllerWrapper cloudsSpin = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 20 * 4)));

    Tween cloudsSpinTween = Tween(begin: -.2, end: .4);
    Animation cloudsSpinAnimationTween =
        cloudsSpinTween.animate(animationController.c);

    cloudsSpin.c.forward();

    zWidgets["cube"] = ZGroup(
      sortMode: SortMode.stack,
      children: [
        ZPositioned(
          rotate: ZVector(pi / 4, 0, 0),
          translate: ZVector(0,
              (MediaQuery.of(globalContext).size.height / 2 - radius / 2), 0),
          child: AnimatedBuilder(
//          key: key,
            animation: animationController.c,
            child: AnimatedBuilder(
              animation: cloudsSpin.c,
              builder: (BuildContext context, Widget child) {
                return ZPositioned.rotate(
                    child: child, z: cloudsSpinAnimationTween.value);
              },
              child: ZGroup(
                children: widgets,
              ),
            ),
            builder: (BuildContext context, Widget child) {
              return ZPositioned(
                  translate: ZVector(0, animateClouds.value, 0),
                  rotate: ZVector(
                      -(pi) -
                          pi / 8 -
                          (pi / 8) * ((animateClouds.value) / radius),
                      0,
                      0),
                  child: child);
            },
          ),
        ),
        AnimatedBuilder(
//          key: key,
          animation: animationController.c,

          child: ZBox(
            width: MediaQuery.of(globalContext).size.height / 4,
            height: MediaQuery.of(globalContext).size.height / 4,
            depth: MediaQuery.of(globalContext).size.height / 4,
            color: Colors.blue.withOpacity(0.3),
            leftColor: Colors.green.withOpacity(0.3),
            rightColor: Colors.amber.withOpacity(0.3),
          ),
          builder: (BuildContext context, Widget child) {
            double coef = 0;
//            if (animationTween2.value > 50){
//              coef = 100.0;
//            }
            return ZToBoxAdapter(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Object3D(
                  zoom: 0.5 +
                      (pow(e,
                          ((1 - animationTween2.value.abs() / 140) * 6) - 4)),
                  path: objectPath,
                  rotate: Vector3(
                      pi + sin(animationTween2.value / 280 * pi),
//                      asin((pi / 8) * asin(2 / animationTween2.value)),
//                      -(4 * pi / 4) + (pi / 8) * asin(2/animationTween2.value),
                      pi * 4 * animationTween2.value / 140,
                      pi * (1 / 60 - random.nextInt((10)).toDouble() / 300)),
                  translate: Vector3(0, animationTween2.value + coef, 0)),
            );
          },
        ),
      ],
    );
    updateScreen();
    await animationController.c.forward();
    zWidgets.remove("cube");
    updateScreen();
    firstForm();
    //fallingIntoPeachTree();
  }

  firstForm() async {
    List<Point> centers = [];
    Point curCenter1 = Point(-MediaQuery.of(globalContext).size.width / 3,
        -MediaQuery.of(globalContext).size.height / 3);
    Point curCenter2 = Point(-MediaQuery.of(globalContext).size.width / 6,
        -MediaQuery.of(globalContext).size.height / 6);
    Point curCenter3 = Point(MediaQuery.of(globalContext).size.width / 3,
        MediaQuery.of(globalContext).size.height / 3);

    double radius = 100;

    ///First group of clouds
    for (int i = 0; i < 360; i += 36) {
      centers.add(Point(curCenter1.x + radius * cos(i * 2 * pi / 360),
          curCenter1.y + radius * sin(i * 2 * pi / 360)));
    }
    List<Widget> firstGroupOfClouds = [];
    for (int i = 0; i < 10; i++) {
      firstGroupOfClouds.add(ZPositioned(
          translate: ZVector(
              centers[i].x + random.nextInt(50).toDouble(),
              centers[i].y + random.nextInt(50).toDouble(),
              random.nextInt(50).toDouble()),
          rotate: ZVector(0, 0, pi * random.nextDouble()),
          child: ZToBoxAdapter(
            width: 256,
            height: 256,
            child: Image.asset(
              "images/sceneFive/smoke.png",
            ),
          )));
      //  widgets.add( position: clouds(centers[i]), rotation: i * 4  );
    }

    ///Second group of clouds
    centers.clear();
    for (int i = 0; i < 360; i += 36) {
      centers.add(Point(curCenter2.x + radius * cos(i * 2 * pi / 360),
          curCenter2.y + radius * sin(i * 2 * pi / 360)));
    }
    List<Widget> secondGroupOfClouds = [];
    for (int i = 0; i < 10; i++) {
      secondGroupOfClouds.add(ZPositioned(
          translate: ZVector(
              centers[i].x + random.nextInt(50).toDouble(),
              centers[i].y + random.nextInt(50).toDouble(),
              random.nextInt(50).toDouble()),
          rotate: ZVector(0, 0, pi * random.nextDouble()),
          child: ZToBoxAdapter(
            width: 256,
            height: 256,
            child: Image.asset(
              "images/sceneFive/smoke.png",
            ),
          )));
      //  widgets.add( position: clouds(centers[i]), rotation: i * 4  );
    }

    ///Third group of clouds
    centers.clear();
    for (int i = 0; i < 360; i += 36) {
      centers.add(Point(curCenter3.x + radius * cos(i * 2 * pi / 360),
          curCenter3.y + radius * sin(i * 2 * pi / 360)));
    }
    List<Widget> thirdGroupOfClouds = [];
    for (int i = 0; i < 10; i++) {
      thirdGroupOfClouds.add(ZPositioned(
          translate: ZVector(
              centers[i].x + random.nextInt(50).toDouble(),
              centers[i].y + random.nextInt(50).toDouble(),
              random.nextInt(50).toDouble()),
          rotate: ZVector(0, 0, pi * random.nextDouble()),
          child: ZToBoxAdapter(
            width: 256,
            height: 256,
            child: Image.asset(
              "images/sceneFive/smoke.png",
            ),
          )));
      //  widgets.add( position: clouds(centers[i]), rotation: i * 4  );
    }

    AControllerWrapper oneCloud = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 5)));

    Tween oneCloudTween = Tween(begin: 0, end: 100);
    Animation oneCloudAnimationTween = oneCloudTween.animate(oneCloud.c);

    AnimationController turbulence =
        AnimationController(vsync: vsync, duration: Duration(milliseconds: 100))
          ..forward(from: 0)
          ..animateTo(1);

    zWidgets["capsule"] = ZGroup(
      children: [
        ZPositioned(
          translate: ZVector(MediaQuery.of(globalContext).size.width / 5,
              MediaQuery.of(globalContext).size.height / 5, 0),
          rotate: ZVector(0, 0, pi - pi / 3),
          child: AnimatedBuilder(
            animation: oneCloud.c,
            builder: (BuildContext context, Widget child) {
              if (turbulence.isCompleted)
                turbulence.animateTo(random.nextDouble());
              return ZToBoxAdapter(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Object3D(
                    zoom: 16.5,
                    path: objectPath,
                    rotate: Vector3(
                        7 * pi / 32,
                        oneCloudAnimationTween.value / 100 * pi * 6,
                        pi * (1 / 60 - turbulence.value.toDouble() * 10 / 300)),
                    translate: Vector3(0, 0, 0)),
              );
            },
          ),
        ),

        AnimatedBuilder(
          animation: oneCloud.c,
          builder: (BuildContext context, Widget child) {
            return ZPositioned.rotate(
              child: child,
            );
          },
          child: ZGroup(
            children: firstGroupOfClouds,
          ),
        ),

        AnimatedBuilder(
          animation: oneCloud.c,
          builder: (BuildContext context, Widget child) {
            return ZPositioned.rotate(
              child: child,
            );
          },
          child: ZGroup(
            children: secondGroupOfClouds,
          ),
        ),
//
//        AnimatedBuilder(
//          animation: oneCloud.c,
//          builder: (BuildContext context, Widget child) {
//            return ZPositioned.rotate(
//              child: child,
//              x: pi / 6,
//            );
//          },
//          child: ZGroup(
//            children: thirdGroupOfClouds,
//          ),
//        ),

//        AnimatedBuilder(
//          animation: oneCloud.c,
//          builder: (BuildContext context, Widget child) {
//            return ZPositioned.rotate(
//              child: child,
//            );
//          },
//          child: ZToBoxAdapter(
//            width: 800,
//            height: 800,
//            child: Image.asset(
//              "images/sceneFive/smoke.png",
//            ),
//          ),
//        ),
      ],
    );

    await oneCloud.c.forward();
    zWidgets.remove("capsule");
    updateScreen();
    //TODO Исправить ошибку SetState() и AnimationController
    await Future.delayed(Duration(milliseconds: 100));
    moreForms();
  }

  moreForms() async {
    List<Point> centers = [];
    Point curCenter1 = Point(0, 0);
//    Point curCenter2 = Point(-MediaQuery.of(globalContext).size.width / 6,
//        -MediaQuery.of(globalContext).size.height / 6);
//    Point curCenter3 = Point(MediaQuery.of(globalContext).size.width / 3,
//        MediaQuery.of(globalContext).size.height / 3);

    double radius = 650;

    AControllerWrapper clouds = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 2)));

    Tween cloudsTween = Tween(begin: 1.0, end: 1.0);
    Animation cloudsAnimationTween = cloudsTween.animate(clouds.c);

    ///First group of clouds
    List<Widget> firstGroupOfClouds = [];
    for (int i = 0; i < 360; i += 6) {
      centers.add(Point(curCenter1.x + radius * cos(i * 2 * pi / 360),
          curCenter1.y + radius * sin(i * 2 * pi / 360)));

      Point one = Point(curCenter1.x + radius * cos(i * 2 * pi / 360),
          curCenter1.y + radius * sin(i * 2 * pi / 360));
      Point two = Point(
          curCenter1.x +
              (MediaQuery.of(globalContext).size.width / 0.8) *
                  cos(i * 2 * pi / 360),
          curCenter1.y +
              (MediaQuery.of(globalContext).size.width / 0.8) *
                  sin(i * 2 * pi / 360));

      Animation xRadius = Tween(begin: one.x, end: two.x).animate(clouds.c);
      Animation yRadius = Tween(begin: one.y, end: two.y).animate(clouds.c);

      double xAdd = random.nextInt(400).toDouble() - 200;
      double yAdd = random.nextInt(400).toDouble() - 200;
      double zAdd = random.nextInt(50).toDouble();

      firstGroupOfClouds.add(AnimatedBuilder(
        child: ZToBoxAdapter(
          width: 1000,
          height: 1000,
          child: Image.asset(
            "images/sceneFive/smoke.png",
            fit: BoxFit.fill,
            width: 1000,
            height: 1000,
          ),
        ),
        builder: (BuildContext context, Widget child) {
          return (ZPositioned(
              translate:
                  ZVector(xRadius.value + xAdd, yRadius.value + yAdd, zAdd),
              rotate: ZVector(0, 0, pi * zAdd / 50),
              child: child));
        },
        animation: clouds.c,
      ));
      //  widgets.add( position: clouds(centers[i]), rotation: i * 4  );
    }

//    for (int i = 0; i < 60; i++) {
//
//    }

    zWidgets["fog"] = ZGroup(
      children: [
        ZGroup(children: firstGroupOfClouds),
        ZPositioned(
            scale: ZVector.all(0.2),
            child: ZGroup(children: firstGroupOfClouds)),
      ],
    );
    updateScreen();
    await clouds.c.forward();
    zWidgets.remove("fog");
    updateScreen();
    await Future.delayed(Duration(milliseconds: 10));
    sameAsBirdInAlice();
  }

  sameAsBirdInAlice() async {
    //TODO Поправить движение капсулы в кадре
    List<Point> centers = [];
    Point curCenter = Point(0, 0);

    double radius = 1200;
    AControllerWrapper capsuleTurbulence = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 10)));

    Tween capsuleTurbulenceTween = Tween(begin: 0, end: 100);
    Animation capsuleTurbulenceAnimationTween =
        capsuleTurbulenceTween.animate(capsuleTurbulence.c);

    AnimationController turbulence =
        AnimationController(vsync: vsync, duration: Duration(milliseconds: 100))
          ..forward(from: 0)
          ..animateTo(1);

    Tween cloudsSpinTween = Tween(begin: 0, end: 100);
    Animation cloudsSpinAnimationTween =
        cloudsSpinTween.animate(capsuleTurbulence.c);

    Tween capsuleMoveTween = Tween(
        begin: -MediaQuery.of(globalContext).size.height * 1.5,
        end: MediaQuery.of(globalContext).size.height * 1.5);
    Animation capsuleMoveAnimationTween =
        capsuleMoveTween.animate(capsuleTurbulence.c);

    Animation capsuleCurveTween = Tween(
            begin: -MediaQuery.of(globalContext).size.height * 1.5,
            end: MediaQuery.of(globalContext).size.height * 1.5)
        .animate(CurvedAnimation(
            parent: capsuleTurbulence.c, curve: Curves.slowMiddle));

    ///Group of clouds
    for (int i = 0; i < 360; i += 10) {
      centers.add(Point(curCenter.x + radius * cos(i * 2 * pi / 360),
          curCenter.y + radius * sin(i * 2 * pi / 360)));
    }

    List<Widget> groupOfClouds = [];
    for (int i = 0; i < 36; i++) {
      groupOfClouds.add(ZPositioned(
          translate: ZVector(
              centers[i].x + random.nextInt(50).toDouble(),
              centers[i].y + random.nextInt(50).toDouble(),
              random.nextInt(50).toDouble()),
          rotate: ZVector(0, 0, pi * random.nextDouble()),
          child: ZToBoxAdapter(
            width: 800,
            height: 800,
            child: Image.asset(
              "images/sceneFive/smoke.png",
              fit: BoxFit.fill,
              width: 800,
              height: 800,
            ),
          )));
      //  widgets.add( position: clouds(centers[i]), rotation: i * 4  );
    }

    zWidgets["asBird"] = ZGroup(
      children: [
//        ZToBoxAdapter(
//          width: 40,
//          height: 40,
//          child: FlutterLogo(),
//        ),

        ///Capsule///
        ZPositioned(
          translate:
              ZVector(-MediaQuery.of(globalContext).size.width / 5, 0, 0),
          rotate: ZVector(pi, 0, pi / 12),
          child: AnimatedBuilder(
            animation: capsuleTurbulence.c,
            builder: (BuildContext context, Widget child) {
              if (turbulence.isCompleted)
                turbulence.animateTo(random.nextDouble());
              return ZToBoxAdapter(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: Object3D(
                    zoom: 13,
                    path: objectPath,
                    rotate: Vector3(
                        7 * pi / 32,
                        capsuleTurbulenceAnimationTween.value / 100 * pi * 6,
                        pi * (1 / 60 - turbulence.value.toDouble() * 10 / 300)),
                    translate: Vector3(
                        capsuleCurveTween.value, -capsuleCurveTween.value, 0)),
              );
            },
          ),
        ),

        ///Clouds///
        AnimatedBuilder(
          animation: capsuleTurbulence.c,
          builder: (BuildContext context, Widget child) {
            return ZPositioned(
              translate: ZVector(-MediaQuery.of(context).size.width / 4, 0, 0),
              rotate: ZVector(0, 0, cloudsSpinAnimationTween.value * pi / 100),
              child: child,
            );
          },
          child: ZGroup(
            children: groupOfClouds,
          ),
        ),
      ],
    );
    updateScreen();
    await capsuleTurbulence.c.forward();
    zWidgets.remove("asBird");
    updateScreen();
    //TODO Исправить ошибку SetState() и AnimationController
    await Future.delayed(Duration(milliseconds: 100));

    freeFallingThroughSkies();
  }

  freeFallingThroughSkies() async {
    List<Point> centers = [];
    //Five enter points

    //Center point one
    double radius1 = 100;
    Point curCenter1 = Point(-MediaQuery.of(globalContext).size.width / 3,
        -MediaQuery.of(globalContext).size.height / 3);

    //Center point two
    double radius2 = 120;
    Point curCenter2 = Point(
        -MediaQuery.of(globalContext).size.width / 2 + radius2 * 2,
//        - MediaQuery.of(globalContext).size.height / 2
        0);

    //Center point three
    double radius3 = 200;
    Point curCenter3 = Point(
        -MediaQuery.of(globalContext).size.width / 3 + radius2 * 3,
        MediaQuery.of(globalContext).size.height / 2);

    //Center point four
    double radius4 = 200;
    Point curCenter4 = Point(
        MediaQuery.of(globalContext).size.width / 3 - radius4 / 2,
        MediaQuery.of(globalContext).size.height / 2 - radius4);

    //Center point five
    double radius5 = 200;
    Point curCenter5 = Point(0, 0);

    //Center point six
    double radius6 = 200;
    Point curCenter6 = Point(0, 0);

    //first group of clouds creating
    for (int i = 0; i < 360; i += 36) {
      centers.add(Point(curCenter1.x + radius1 * cos(i * 2 * pi / 360),
          curCenter1.y + radius1 * sin(i * 2 * pi / 360)));
    }
    List<Widget> firstGroupOfClouds = [];
    for (int i = 0; i < 10; i++) {
      firstGroupOfClouds.add(ZPositioned(
          translate: ZVector(
              centers[i].x + random.nextInt(50) - 25,
              centers[i].y + random.nextInt(100) - 50,
              random.nextInt(50).toDouble()),
          rotate: ZVector(pi / 6, 0, random.nextDouble() * pi),
          child: ZToBoxAdapter(
            width: 256,
            height: 256,
            child: Image.asset(
              "images/sceneFive/smoke.png",
              fit: BoxFit.fill,
              width: 256,
              height: 256,
            ),
          )));
    }
    centers.clear();

    //second group of clouds creating
    for (int i = 0; i < 360; i += 36) {
      centers.add(Point(curCenter2.x + radius2 * cos(i * 2 * pi / 360),
          curCenter2.y + radius2 * sin(i * 2 * pi / 360)));
    }
    List<Widget> secondGroupOfClouds = [];
    for (int i = 0; i < 10; i++) {
      secondGroupOfClouds.add(ZPositioned(
          translate: ZVector(
              centers[i].x + random.nextInt(100) - 50,
              centers[i].y + random.nextInt(100) - 50,
              random.nextInt(50).toDouble()),
          rotate: ZVector(pi / 6, 0, random.nextDouble() * pi),
          child: ZToBoxAdapter(
            width: 400,
            height: 400,
            child: Image.asset(
              "images/sceneFive/smoke.png",
              fit: BoxFit.fill,
              width: 400,
              height: 400,
            ),
          )));
    }
    centers.clear();

    //third group of clouds creating
    for (int i = 0; i < 360; i += 36) {
      centers.add(Point(curCenter3.x + radius3 * cos(i * 2 * pi / 360),
          curCenter3.y + radius3 * sin(i * 2 * pi / 360)));
    }
    List<Widget> thirdGroupOfClouds = [];
    for (int i = 0; i < 10; i++) {
      thirdGroupOfClouds.add(ZPositioned(
          translate: ZVector(
              centers[i].x + random.nextInt(50) - 25,
              centers[i].y + random.nextInt(200) - 100,
              random.nextInt(50).toDouble()),
          rotate: ZVector(pi / 6, 0, random.nextDouble() * pi),
          child: ZToBoxAdapter(
            width: 600,
            height: 600,
            child: Image.asset(
              "images/sceneFive/smoke.png",
              fit: BoxFit.fill,
              width: 600,
              height: 600,
            ),
          )));
    }
    centers.clear();

    //fourth group of clouds creating
    for (int i = 0; i < 360; i += 36) {
      centers.add(Point(curCenter4.x + radius4 * cos(i * 2 * pi / 360),
          curCenter4.y + radius4 * sin(i * 2 * pi / 360)));
    }
    List<Widget> fourthGroupOfClouds = [];
    for (int i = 0; i < 10; i++) {
      fourthGroupOfClouds.add(ZPositioned(
          translate: ZVector(
              centers[i].x + random.nextInt(50) - 25,
              centers[i].y + random.nextInt(50) - 25,
              random.nextInt(50).toDouble()),
          rotate: ZVector(pi / 6, 0, random.nextDouble() * pi),
          child: ZToBoxAdapter(
            width: 400,
            height: 600,
            child: Image.asset(
              "images/sceneFive/smoke.png",
              fit: BoxFit.fill,
              width: 400,
              height: 600,
            ),
          )));
    }
    centers.clear();

// Animation Tweens
    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 10)));

    Tween capsuleMove = Tween(
        begin: (MediaQuery.of(globalContext).size.height + radius1 * 2),
        end: (MediaQuery.of(globalContext).size.height / 2 - radius1 / 2));
    Animation capsuleMoveAnimationTween =
        capsuleMove.animate(animationController.c);

    Animation cloudsSpin =
        Tween(begin: 0, end: 100.0).animate(animationController.c);

    animationController.c.forward();

    AnimationController turbulence =
        AnimationController(vsync: vsync, duration: Duration(milliseconds: 100))
          ..forward(from: 0)
          ..animateTo(1);

// Animation Tweens ^

    zWidgets["capsuleInClouds"] = ZGroup(
      sortMode: SortMode.stack,
      children: [
        //First group of clouds
        ZPositioned(
          rotate: ZVector(0, 0, 0),
          translate: ZVector(0, 0, 0),
          child: AnimatedBuilder(
            animation: animationController.c,
            child: AnimatedBuilder(
              animation: animationController.c,
              builder: (BuildContext context, Widget child) {
                return ZPositioned(
                  rotate: ZVector(0, 0, 0),
                  child: child,
                );
              },
              child: ZGroup(
                children: firstGroupOfClouds,
              ),
            ),
            builder: (BuildContext context, Widget child) {
              return ZPositioned(
                  translate: ZVector(0, -cloudsSpin.value * 2, 0),
                  //translate: ZVector(0, 0, 0),
                  rotate: ZVector(
                      pi / 16 + (pi / 8) * ((cloudsSpin.value) / radius1),
                      0,
                      0),
                  child: child);
            },
          ),
        ),

        //Second group of clouds
        ZPositioned(
          rotate: ZVector(0, 0, 0),
          translate: ZVector(0, 0, 0),
          child: AnimatedBuilder(
            animation: animationController.c,
            child: AnimatedBuilder(
              animation: animationController.c,
              builder: (BuildContext context, Widget child) {
                return ZPositioned(
                  rotate: ZVector(0, 0, 0),
                  child: child,
                );
              },
              child: ZGroup(
                children: secondGroupOfClouds,
              ),
            ),
            builder: (BuildContext context, Widget child) {
              return ZPositioned(
                  translate: ZVector(0, -cloudsSpin.value * 2, 0),
                  //translate: ZVector(0, 0, 0),
                  rotate: ZVector(
                      pi / 16 + (pi / 8) * ((cloudsSpin.value) / radius1),
                      0,
                      0),
                  child: child);
            },
          ),
        ),

        //Third group of clouds
        ZPositioned(
          rotate: ZVector(0, 0, 0),
          translate: ZVector(0, 0, 0),
          child: AnimatedBuilder(
            animation: animationController.c,
            child: AnimatedBuilder(
              animation: animationController.c,
              builder: (BuildContext context, Widget child) {
                return ZPositioned(
                  rotate: ZVector(0, 0, 0),
                  child: child,
                );
              },
              child: ZGroup(
                children: thirdGroupOfClouds,
              ),
            ),
            builder: (BuildContext context, Widget child) {
              return ZPositioned(
                  translate: ZVector(0, -cloudsSpin.value * 2, 0),
                  //translate: ZVector(0, 0, 0),
                  rotate: ZVector(
                      //pi / 16 +
                      (pi / 8) * ((cloudsSpin.value) / radius1),
                      0,
                      0),
                  child: child);
            },
          ),
        ),

        //Fourth group of clouds
        ZPositioned(
          rotate: ZVector(0, 0, 0),
          translate: ZVector(0, 0, 0),
          child: AnimatedBuilder(
            animation: animationController.c,
            child: AnimatedBuilder(
              animation: animationController.c,
              builder: (BuildContext context, Widget child) {
                return ZPositioned(
                  rotate: ZVector(0, 0, 0),
                  child: child,
                );
              },
              child: ZGroup(
                children: fourthGroupOfClouds,
              ),
            ),
            builder: (BuildContext context, Widget child) {
              return ZPositioned(
                  translate: ZVector(0, -cloudsSpin.value * 2, 0),
                  //translate: ZVector(0, 0, 0),
                  rotate: ZVector(
                      pi / 16 - (pi / 8) * ((cloudsSpin.value) / radius1),
                      0,
                      0),
                  child: child);
            },
          ),
        ),

        //Capsule

        ZPositioned(
          rotate: ZVector(pi, 0, 0),
          child: AnimatedBuilder(
            animation: animationController.c,
            child: ZBox(
              width: MediaQuery.of(globalContext).size.height / 4,
              height: MediaQuery.of(globalContext).size.height / 4,
              depth: MediaQuery.of(globalContext).size.height / 4,
              color: Colors.blue.withOpacity(0.3),
              leftColor: Colors.green.withOpacity(0.3),
              rightColor: Colors.amber.withOpacity(0.3),
            ),
            builder: (BuildContext context, Widget child) {
              if (turbulence.isCompleted)
                turbulence.animateTo(random.nextDouble());
              return ZToBoxAdapter(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Object3D(
                    zoom: 0.5 +
                        (pow(
                            e,
                            ((1 - capsuleMoveAnimationTween.value.abs() / 140) *
                                    6) -
                                4)),
                    path: objectPath,
                    rotate: Vector3(
                        0,
                        capsuleMoveAnimationTween.value / 100 * pi * 6,
//                      pi * (1 / 60 - cloudsSpin.value.toDouble() / 300)
                        pi * (1 / 60 - turbulence.value.toDouble() * 10 / 300)),
                    translate: Vector3(0, 0, 0)),
              );
            },
          ),
        ),
      ],
    );

    updateScreen();
    await animationController.c.forward();
    zWidgets.remove("capsuleInClouds");
    updateScreen();
    asPeachTree();
    //TODO Исправить ошибку SetState() и AnimationController
    await Future.delayed(Duration(milliseconds: 100));
  }

  asPeachTree() async {
    List<Point> centers = [];
    Point curCenter1 = Point(0, 0);

    double radius = 500;

    //first group of clouds creating
    for (int i = 0; i < 360; i += 24) {
      centers.add(Point(curCenter1.x + radius * cos(i * 2 * pi / 360),
          curCenter1.y + radius * sin(i * 2 * pi / 360)));
    }
    List<Widget> firstGroupOfClouds = [];
    for (int i = 0; i < 15; i++) {
      firstGroupOfClouds.add(ZPositioned(
          translate: ZVector(
              centers[i].x + random.nextInt(100) - 50,
              centers[i].y + random.nextInt(100) - 50,
              random.nextInt(50).toDouble() - 100),
          rotate: ZVector(0, 0, random.nextDouble() * pi),
          child: ZToBoxAdapter(
            width: 400,
            height: 400,
            child: Image.asset(
              "images/sceneFive/smoke.png",
              fit: BoxFit.fill,
              width: 400,
              height: 400,
            ),
          )));
    }
    centers.clear();

    //Second group of clouds creating
    radius = 200;
    for (int i = 0; i < 360; i += 36) {
      centers.add(Point(curCenter1.x + radius * cos(i * 2 * pi / 360),
          curCenter1.y + radius * sin(i * 2 * pi / 360)));
    }
    List<Widget> secondGroupOfClouds = [];
    for (int i = 0; i < 10; i++) {
      secondGroupOfClouds.add(ZPositioned(
          translate: ZVector(
              centers[i].x + random.nextInt(100) - 50,
              centers[i].y + random.nextInt(100) - 50,
              random.nextInt(50).toDouble() - 100),
          rotate: ZVector(0, 0, random.nextDouble() * pi),
          child: ZToBoxAdapter(
            width: 600,
            height: 800,
            child: Image.asset(
              "images/sceneFive/smoke.png",
              fit: BoxFit.fill,
              width: 600,
              height: 800,
            ),
          )));
    }
    centers.clear();

    //Third group of clouds creating
    radius = 700;
    for (int i = 0; i < 360; i += 22) {
      centers.add(Point(curCenter1.x + radius * cos(i * 2 * pi / 360),
          curCenter1.y + radius * sin(i * 2 * pi / 360)));
    }
    List<Widget> thirdGroupOfClouds = [];
    for (int i = 0; i < 16; i++) {
      thirdGroupOfClouds.add(ZPositioned(
          translate: ZVector(
              centers[i].x + random.nextInt(100) - 50,
              centers[i].y + random.nextInt(100) - 50,
              random.nextInt(50).toDouble() - 100),
          rotate: ZVector(0, 0, random.nextDouble() * pi),
          child: ZToBoxAdapter(
            width: 1000,
            height: 1000,
            child: Image.asset(
              "images/sceneFive/smoke.png",
              fit: BoxFit.fill,
              width: 1000,
              height: 1000,
            ),
          )));
    }
    centers.clear();

    AControllerWrapper clouds = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 4)));

    Tween cloudsTween = Tween(begin: 1.0, end: 100.0);
    Animation cloudsAnimationTween = cloudsTween.animate(clouds.c);

    ///Only two clouds
    List<Widget> onlyOneCloud = [];

    for (int i = 0; i < 5; i++) {
      centers.add(Point(curCenter1.x, curCenter1.y));

      Point one = Point(curCenter1.x, curCenter1.y);

      Animation yRadius = Tween(begin: 800.0, end: 2400.0).animate(clouds.c);
      Animation xRadius = Tween(begin: 800.0, end: 2400.0).animate(clouds.c);

      double xAdd = random.nextInt(100).toDouble() - 50;
      double yAdd = random.nextInt(100).toDouble() - 50;
      double zAdd = random.nextInt(50).toDouble();

      onlyOneCloud.add(AnimatedBuilder(
        builder: (BuildContext context, Widget child) {
          return (ZPositioned(
            translate: ZVector(one.x + xAdd, one.y + yAdd, zAdd),
            rotate: ZVector(0, 0, pi * zAdd / 25),
            child: ZToBoxAdapter(
              width: xRadius.value,
              height: yRadius.value,
              child: Image.asset(
                "images/sceneFive/smoke.png",
                fit: BoxFit.fill,
                width: 1600,
                height: 1600,
              ),
            ),
          ));
        },
        animation: clouds.c,
      ));
    }

    zWidgets["peachTree"] = ZGroup(
      sortMode: SortMode.stack,
      children: [
        AnimatedBuilder(
          animation: clouds.c,
          child: ZGroup(children: [
            ZPositioned(
              rotate: ZVector(0, 0, 0),
              translate: ZVector(0, 0, 0),
              child: AnimatedBuilder(
                animation: clouds.c,
                child: AnimatedBuilder(
                  animation: clouds.c,
                  builder: (BuildContext context, Widget child) {
                    return ZPositioned(
                      rotate: ZVector(0, 0, 0),
                      child: child,
                    );
                  },
                  child: ZGroup(
                    children: firstGroupOfClouds,
                  ),
                ),
                builder: (BuildContext context, Widget child) {
                  return ZPositioned(
                      //translate: ZVector(0, -cloudsAnimationTween.value * 2, 0),
                      //translate: ZVector(0, 0, 0),
                      rotate: ZVector(
                          pi / 8 -
                              (pi / 8) * ((cloudsAnimationTween.value) / 100),
                          0,
                          //pi / 8 - (pi / 8) * ((cloudsAnimationTween.value) / 100),
                          //-cloudsAnimationTween.value / 400 * pi
                          0),
                      child: child);
                },
              ),
            ),
            ZPositioned(
              rotate: ZVector(0, 0, 0),
              translate: ZVector(0, 0, 0),
              child: AnimatedBuilder(
                animation: clouds.c,
                child: AnimatedBuilder(
                  animation: clouds.c,
                  builder: (BuildContext context, Widget child) {
                    return ZPositioned(
                      rotate: ZVector(0, 0, 0),
                      child: child,
                    );
                  },
                  child: ZGroup(
                    children: secondGroupOfClouds,
                  ),
                ),
                builder: (BuildContext context, Widget child) {
                  return ZPositioned(
                      //translate: ZVector(0, -cloudsAnimationTween.value * 2, 0),
                      //translate: ZVector(0, 0, 0),
                      rotate: ZVector(
                          pi / 8 -
                              (pi / 8) * ((cloudsAnimationTween.value) / 100),
                          0,
                          //pi / 8 - (pi / 8) * ((cloudsAnimationTween.value) / 100),
                          //-cloudsAnimationTween.value / 400 * pi
                          0),
                      child: child);
                },
              ),
            ),
            ZPositioned(
              rotate: ZVector(0, 0, 0),
              translate: ZVector(0, 0, 0),
              child: AnimatedBuilder(
                animation: clouds.c,
                child: AnimatedBuilder(
                  animation: clouds.c,
                  builder: (BuildContext context, Widget child) {
                    return ZPositioned(
                      rotate: ZVector(0, 0, 0),
                      child: child,
                    );
                  },
                  child: ZGroup(
                    children: thirdGroupOfClouds,
                  ),
                ),
                builder: (BuildContext context, Widget child) {
                  return ZPositioned(
                      //translate: ZVector(0, -cloudsAnimationTween.value * 2, 0),
                      //translate: ZVector(0, 0, 0),
                      rotate: ZVector(
                          pi / 8 -
                              (pi / 8) * ((cloudsAnimationTween.value) / 100),
                          0,
                          //pi / 8 - (pi / 8) * ((cloudsAnimationTween.value) / 100),
                          //-cloudsAnimationTween.value / 400 * pi
                          0),
                      child: child);
                },
              ),
            ),
          ]),
          builder: (context, child) {
            return ZPositioned(
              rotate: ZVector(0, 0, cloudsAnimationTween.value / 400 * pi),
              child: child,
            );
          },
        ),
        ZPositioned(child: ZGroup(children: onlyOneCloud)),
      ],
    );
    updateScreen();
    await clouds.c.forward();
    zWidgets.remove("peachTree");
    updateScreen();
    await Future.delayed(Duration(milliseconds: 10));
    fallingIntoPeachTree();
  }

  fallingIntoPeachTree() async {
    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 10)));

    Tween tween = Tween(
        begin: (MediaQuery.of(globalContext).size.height + 750 * 2),
        end: (MediaQuery.of(globalContext).size.height / 2 - 750 / 2));

    Animation animationTween2 =
        Tween(begin: -140, end: 190.0).animate(animationController.c);

    Animation animationTween3 =
        Tween(begin: 0, end: 100.0).animate(animationController.c);

    double radius = 650;

    Point curCenter1 = Point(0, 0);
    List<Point> centers = [];

    ///Group of clouds

    AControllerWrapper clouds = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 10)));

    Tween cloudsTween = Tween(begin: 1.0, end: 1.0);
    Animation cloudsAnimationTween = cloudsTween.animate(clouds.c);

    List<Widget> firstGroupOfClouds = [];
    for (int i = 0; i < 360; i += 36) {
      Point one = Point(
          curCenter1.x +
              (MediaQuery.of(globalContext).size.width / 2) *
                  cos((i) * 2 * pi / 360),
          curCenter1.y +
              (MediaQuery.of(globalContext).size.width / 2) *
                  sin((i) * 2 * pi / 360));
      Point two = Point(
          curCenter1.x +
              (2 * radius + MediaQuery.of(globalContext).size.width / 2) *
                  cos((i) * 2 * pi / 360),
          curCenter1.y +
              (2 * radius + MediaQuery.of(globalContext).size.width / 2) *
                  sin((i) * 2 * pi / 360));

      Animation xRadius = Tween(begin: two.x, end: one.x).animate(clouds.c);
      Animation yRadius = Tween(begin: two.y, end: one.y).animate(clouds.c);

      double xAdd = random.nextInt(400).toDouble() - 200;
      double yAdd = random.nextInt(400).toDouble() - 200;
      double zAdd = random.nextInt(50).toDouble();

      AControllerWrapper cloudsSpin = AControllerWrapper(AnimationController(
          vsync: vsync, duration: Duration(seconds: 20 * 4)));

      firstGroupOfClouds.add(AnimatedBuilder(
        child: ZToBoxAdapter(
          width: 1000,
          height: 1000,
          child: Image.asset(
            "images/sceneFive/smoke.png",
            fit: BoxFit.fill,
            width: 1000,
            height: 1000,
          ),
        ),
        builder: (BuildContext context, Widget child) {
          return (ZPositioned(
              translate:
                  ZVector(xRadius.value + xAdd, yRadius.value + yAdd, zAdd),
              rotate: ZVector(0, 0, 0),
              child: child));
        },
        animation: clouds.c,
      ));
    }

    zWidgets["intoPeach"] = ZGroup(
      sortMode: SortMode.stack,
      children: [
        ZPositioned(
          rotate: ZVector(0, 0, 0),
          translate: ZVector(0, 0, 0),
          child: AnimatedBuilder(
            animation: clouds.c,
            child: AnimatedBuilder(
              animation: clouds.c,
              builder: (BuildContext context, Widget child) {
                return ZPositioned(child: child);
              },
              child: ZGroup(
                children: firstGroupOfClouds,
              ),
            ),
            builder: (BuildContext context, Widget child) {
              return ZPositioned(
                  translate: ZVector(0, 0, 0),
                  rotate: ZVector(0, 0, 0),
                  child: child);
            },
          ),
        ),

        /// Capsule ///
        AnimatedBuilder(
          animation: animationController.c,
          child: ZBox(
            width: MediaQuery.of(globalContext).size.height / 4,
            height: MediaQuery.of(globalContext).size.height / 4,
            depth: MediaQuery.of(globalContext).size.height / 4,
            color: Colors.blue.withOpacity(0.3),
            leftColor: Colors.green.withOpacity(0.3),
            rightColor: Colors.amber.withOpacity(0.3),
          ),
          builder: (BuildContext context, Widget child) {
            return ZToBoxAdapter(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Object3D(
                  zoom: (animationTween2.value + 141) / 20,
                  path: objectPath,
                  rotate: Vector3(
                      pi - pi / 6,
                      pi * 4 * animationTween2.value / 140,
                      pi * (1 / 60 - random.nextInt((10)).toDouble() / 300)),
                  translate: Vector3(
                      animationTween3.value * 1.5 -
                          MediaQuery.of(globalContext).size.width / 12,
                      animationTween2.value +
                          150 -
                          MediaQuery.of(globalContext).size.width / 12,
                      0)),
            );
          },
        ),
      ],
    );

    clouds.c.forward();
    updateScreen();
    await animationController.c.forward();
    zWidgets.remove("intoPeach");
    updateScreen();
    disappearing();
  }

  disappearing() async {
    ///Animation controller for opacity

    AControllerWrapper forOpacity = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 2)));

    Tween cloudsOpacity = Tween(begin: 1, end: 0);
    Animation cloudsOpacityAnimationTween = cloudsOpacity.animate(forOpacity.c);

    ///Animation controller for opacity ^

    List<Point> centers = [];
    Point curCenter = Point(0, 0);
    double radius = 200;

    for (int i = 0; i < 360; i += 8) {
      centers.add(Point(curCenter.x + radius * cos(i * 2 * pi / 360),
          curCenter.y + radius * sin(i * 2 * pi / 360)));
    }
    List<Widget> widgets = [];
    for (int i = 0; i < 45; i++) {
      widgets.add(ZPositioned(
          translate: ZVector(
              centers[i].x - 350 + random.nextInt(700),
              centers[i].y - 50 + random.nextInt(100),
              random.nextInt(50).toDouble()),
          rotate: ZVector(2, 0, random.nextDouble() * pi / 2),
          child: ZToBoxAdapter(
            width: 256,
            height: 256,
            child: AnimatedBuilder(
                animation: forOpacity.c,
                builder: (context, snapshot) {
                  return Opacity(
                    opacity: cloudsOpacityAnimationTween.value,
                    child: Image.asset(
                      "images/sceneFive/smoke.png",
                    ),
                  );
                }),
          )));
    }

    Key key = GlobalKey();

    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 20 * 2)));

    Tween tween = Tween(begin: -.2, end: .4);
    Animation animationTween = tween.animate(animationController.c);

    zWidgets["clouds"] = AnimatedBuilder(
      key: key,
      animation: animationController.c,
      child: ZGroup(
        children: widgets,
      ),
      builder: (BuildContext context, Widget child) {
        return ZPositioned(
            translate: ZVector(0, -MediaQuery.of(context).size.height / 2.5, 0),
            rotate: ZVector(pi / 4, 0, animationTween.value),
            child: child);
      },
    );
    updateScreen();
    animationController.c.forward();
    capsule().then((value) {
      animationController.close();
      forOpacity.c.forward();
      //zWidgets.remove("clouds");
      key = null;
      updateScreen();
      //spaceCube();
    });

//    while((await Screen.broadcaster.firstWhere((element) => element=="secondCadr"))){
//
//    }
  }

  Future capsule() async {
    Key key = GlobalKey();

    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 12)));

    Tween tween = Tween(
        begin: -MediaQuery.of(globalContext).size.height,
        end: MediaQuery.of(globalContext).size.height / 2.01);
    Animation animationTween = tween.animate(animationController.c);

    zWidgets["capsule"] = AnimatedBuilder(
      key: key,
      animation: animationController.c,
      child: ZToBoxAdapter(
        width: 20,
        height: 20,
        child: SpaceShip(),
      ),
      builder: (BuildContext context, Widget child) {
        return ZPositioned(
            //rotate: ZVector(0,0,random.nextDouble()),
            translate: ZVector(next(-1, 2).toDouble(), animationTween.value, 0),
            child: child);
      },
    );
    updateScreen();
    await animationController.c.forward();
    //zWidgets.remove("capsule");
    key = null;
    return;
  }

  bunnyVideo() async {
    AControllerWrapper animationController = AControllerWrapper(
        AnimationController(vsync: vsync, duration: Duration(seconds: 10)));

    Tween capsuleMove = Tween(begin: 0, end: 1);

    Animation capsuleMoveAnimationTween =
        capsuleMove.animate(animationController.c);

    Animation cloudsSpin =
        Tween(begin: 0, end: 100.0).animate(animationController.c);

    animationController.c.forward();
    VideoPlayerController _controller = VideoPlayerController.network(
        'http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4');

    _controller.setVolume(0);

    zWidgets["video"] = ZGroup(
      children: [
        ///Capsule///
        AnimatedBuilder(

          child: VideoPlayer(_controller),
          animation: animationController.c,
          builder: (BuildContext context, Widget child) {

            _controller.setVolume(capsuleMoveAnimationTween.value);

            return ZPositioned(
              rotate: ZVector(0, 0, capsuleMoveAnimationTween.value * pi),
              child: ZToBoxAdapter(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                child: child,

              ),
            );
          },
        ),
      ],
    );
    updateScreen();
    await _controller.initialize();
    updateScreen();
    _controller.play();
  }

}

class SpaceShip extends StatefulWidget {
  @override
  _SpaceShipState createState() => _SpaceShipState();
}

class _SpaceShipState extends State<SpaceShip> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Object3D(
        translate: Vector3(0, 0, 0),
        rotate: Vector3(0, 0, pi),
        zoom: .2,
        path: objectPath);
  }
}
