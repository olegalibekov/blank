import 'dart:math';

import 'package:blank/render_lib/point_3d.dart';
import 'package:flutter/material.dart';
import 'package:zflutter/zflutter.dart';

class Shapes {

  static List<ZShape> quadrilateralSides(Point3D Q, double a, double b, double opacityValue) {
    List<ZShape> planes = [];
    ZShape plane = ZShape(
      color: Colors.white.withOpacity(opacityValue),
      path: [
        ZMove.only(x: Q.x - a / 2, y: Q.y + b / 2, z: Q.z),
        ZLine.only(x: Q.x + a / 2, y: Q.y + b / 2, z: Q.z),
        ZLine.only(x: Q.x + a / 2, y: Q.y - b / 2, z: Q.z),
        ZLine.only(x: Q.x - a / 2, y: Q.y - b / 2, z: Q.z),
      ],
      closed: true,
      stroke: 0,
      fill: true,
    );
    planes.add(plane);
    return planes;
  }
}
