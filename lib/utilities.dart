import 'dart:collection';
import 'dart:math';

import 'package:blank/scenes/scene_one.dart';
import 'package:blank/screen.dart';
import 'package:flutter/cupertino.dart';
import 'dart:async';

extension IconExtension on SceneOneEvents {
  String i() {
    return toString();
  }
}

Duration animationDuration = Duration(milliseconds: 300);
Duration animationDurationX2 = Duration(milliseconds: 600);
Duration animationDurationX3 = Duration(milliseconds: 1200);
Duration animationDurationX4 = Duration(milliseconds: 1800);
Duration animationDurationX5 = Duration(milliseconds: 2100);
Duration animationDurationX6 = Duration(milliseconds: 2400);
Duration animationDurationX7 = Duration(milliseconds: 2700);

animatedSwitcher(Widget child,
    [Duration duration, switchInCurve, switchOutCurve]) {
  return AnimatedSwitcher(
    duration: duration ?? animationDuration,
    child: child,
    switchInCurve: switchInCurve ?? Curves.linear,
    switchOutCurve: switchOutCurve ?? Curves.linear,
  );
}

class AControllerWrapper {
  AnimationController c;
  Key key;

  AControllerWrapper(AnimationController animationController) {
    this.c = animationController;
    this.key = UniqueKey();
    Screen.animationControllers[key] = c;
  }

  AnimationController call() {
    return c;
  }

  close() {
    Screen.animationControllers.remove(key);
    this.c = null;
    this.key = null;
  }
}

extension extraMove<T, V> on Map<T, V> {
  moveTo(T key, int position) {
    if (!this.containsKey(key)) return;
    var object = this[key];
    this.remove(key);
    //LinkedHashMap().
  }

  moveToFirst(Object key) {}

  moveToLast(String key) {}
}

CoolTimer addTimer(Duration duration, Function callback) {
  Key key = UniqueKey();
  Screen.timers[key] = (CoolTimer(key, animationDurationX3, callback));
  return Screen.timers[key];
}

middleTimer(Duration duration) async {
  String _key = getRandomString(10);
  CoolTimer(UniqueKey(), duration, () => Screen.brain.add(_key));
  await Screen.broadcaster.firstWhere((event) => event == _key);
}

class CoolTimer {
  Duration duration;
  final Function callback;
  final Key key;
  Timer timer;

  Duration started, ended;

  CoolTimer(this.key, this.duration, this.callback) {
    timer = Timer(duration, () {
      Screen.timers.remove(key);
      callback();
    });
    started = Screen.stopwatch.elapsed;
  }

  void pause() {
    timer.cancel();
    ended = Screen.stopwatch.elapsed;
    duration = duration - (ended - started);
  }

  void resume() {
    timer = Timer(duration, () {
      Screen.timers.remove(key);
      callback();
    });
  }
}

const _chars = 'AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1234567890';
Random _rnd = Random();

String getRandomString(int length) => String.fromCharCodes(Iterable.generate(
    length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

class PictureInformation {
  String image;
  int width;
  int height;
  LeftUpper leftUpper;

  PictureInformation({this.image, this.width, this.height, this.leftUpper});

  PictureInformation.fromJson(Map<String, dynamic> json) {
    image = json['image'];
    width = json['width'];
    height = json['height'];
    leftUpper = json['left_upper'] != null
        ? new LeftUpper.fromJson(json['left_upper'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = this.image;
    data['width'] = this.width;
    data['height'] = this.height;
    if (this.leftUpper != null) {
      data['left_upper'] = this.leftUpper.toJson();
    }
    return data;
  }
}

class LeftUpper {
  int x;
  int y;

  LeftUpper({this.x, this.y});

  LeftUpper.fromJson(Map<String, dynamic> json) {
    x = json['x'];
    y = json['y'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['x'] = this.x;
    data['y'] = this.y;
    return data;
  }
}

extension jsonParse on Point {
  fromJson(Map<String, dynamic> json) {
    return Point(json["x"] as double,json["y"] as double);
  }
}
