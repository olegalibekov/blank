import 'dart:async';

import 'package:blank/scenes/scene_one.dart';
import 'package:blank/scenes/scene_two.dart';
import 'package:blank/scenes/scene_seven.dart';
import 'package:blank/scenes/scene_three/scene_three.dart';
import 'package:blank/scenes/scene_five.dart';
import 'package:blank/utilities.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:zflutter/zflutter.dart';

enum GlobalEvents { play, pause, fullStop }

class ScreenWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ScreenWidgetState();
}

class _ScreenWidgetState extends State<ScreenWidget>
    with Screen, TickerProviderStateMixin {
  @override
  void initState() {
    vsync = this;
    Screen.setStateController.stream.listen((event) {
      setState(() {});
    });
    launchBrain();
    super.initState();
  }

  @override
  void dispose() {
    Screen.setStateController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    globalContext = context;
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: Stack(
          children: [
            if (backgroundWidget != null) backgroundWidget,
//            if (zWidgets.isNotEmpty)
//              ZDragDetector(
//                builder: (context, controller) {
//                  return ZIllustration(children: [
//                    ZPositioned(
//                        rotate: controller.rotate,
//                        child: ZGroup(
//                          children: [...zWidgets.values],
//                        ))
//                  ]);
//                },
//              ),
            if (zWidgets.isNotEmpty)
              ZIllustration(children: [...zWidgets.values]),
            if (positionedWidgets.isNotEmpty) ...positionedWidgets.values,
            if (startSequenceWidget != null)
              AnimatedSwitcher(
                  duration: animationDuration, child: startSequenceWidget),

            IgnorePointer(
              child: AppBar(
                backgroundColor: Colors.transparent,
                elevation: 0,
                title: Text('blank'),
              ),
            ),
            //AnimatedSwitcher(duration: null)
          ],
        ));
  }
}

class Screen {
  static StreamController brain = StreamController();
  static Stream broadcaster = brain.stream.asBroadcastStream();
  static StreamController setStateController = StreamController();
  static Map<Key, CoolTimer> timers = {};
  static Map<Key, AnimationController> animationControllers = {};
  static Stopwatch stopwatch = Stopwatch();
  Timer lastEvent;

  TickerProvider vsync;
  BuildContext globalContext;

  static const Duration maxEventDelay = Duration(seconds: 120);

  launchBrain() {
    broadcaster.listen((event) {
      print(
          'event: @$event at ${DateTime.now()}\nTime elapsed from start - ${stopwatch.elapsed}\n');

      if (event == GlobalEvents.pause) {
        stopwatch.stop();
        timers.forEach((key, timer) {
          timer.pause();
        });
        animationControllers.forEach((key, animationController) {
          animationController.stop();
        });
      }
      if (event == GlobalEvents.play) {
        stopwatch.start();
        timers.forEach((key, timer) {
          timer.resume();
        });
        animationControllers.forEach((key, animationController) {
          animationController.forward();
        });
      }

      if (lastEvent != null) lastEvent.cancel();
      lastEvent = Timer(maxEventDelay, () {
        brain.add(GlobalEvents.fullStop);
      });
      if (event == GlobalEvents.fullStop) {
        stopwatch.stop();
        if (lastEvent != null) lastEvent.cancel();
        lastEvent = null;
        timers.forEach((key, timer) {
          timer.pause();
        });
        timers = {};
      }
    });
    backgroundWidget = animatedSwitcher(Container());
    startSequenceWidget = StartSequence(key: UniqueKey());
    initSceneOne();
    initSceneTwo();
    initSceneThree();
    initSceneFive();
    initSceneSeven();
  }

  updateScreen() {
    setStateController.add(true);
  }

  Widget backgroundWidget;
  Widget startSequenceWidget;

  Map<String, Widget> vgWidgets = {};
  Map<String, Widget> vzWidgets = {};
  Map<String, Widget> zWidgets = {};
  Map<String, Widget> positionedWidgets = {};
}

List<Animation> zTween(
    List point1, List point2, AnimationController animationController) {
  Animation x =
      Tween(begin: point1[0], end: point2[0]).animate(animationController);
  Animation y =
      Tween(begin: point1[1], end: point2[1]).animate(animationController);
  Animation z =
      Tween(begin: point1[2], end: point2[2]).animate(animationController);

  return [x, y, z];
}
